PREFIX?=/usr/local
BINDIR=$(DESTDIR)$(PREFIX)/bin
LIBDIR=$(DESTDIR)${PREFIX}/lib/jailer

CP=/bin/cp
INSTALL=/usr/bin/install
MKDIR=/bin/mkdir

PROG=jailer

install:
	$(MKDIR) -p $(BINDIR)
	$(INSTALL) -m 544 $(PROG) $(BINDIR)/

	$(MKDIR) -p $(LIBDIR)
	$(INSTALL) lib/* $(LIBDIR)